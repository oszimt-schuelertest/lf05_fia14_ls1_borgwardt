import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    Scanner tastatur = new Scanner(System.in);
       double zuZahlenderBetrag = 0; 
       double eingezahlterGesamtbetrag = 0;
       boolean wiederholen = true;
       String[] fahrkarten = {"", "Einzelfahrschein Berlin AB	", "Einzelfahrschein Berlin BC	", "Einzelfahrschein Berlin ABC	", "Kurzstrecke			", "Tageskarte Berlin AB		", "Tageskarte Berlin BC		", "Tageskarte Berlin ABC		", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
       Double[] fahrkartenpreise = {0.0, 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
       while(wiederholen) {
       zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur, fahrkarten, fahrkartenpreise);
       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);
       fahrkartenAusgeben();
       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
       System.out.print("\n\n\nWollen sie ein weiteres Ticket kaufen? (j/n)");
       char weiter = tastatur.next().charAt(0);
       if(weiter == 'n' || weiter == 'N') {
    	   wiederholen = false;
       }
       }
    }
    
    public static double fahrkartenbestellungErfassen(Scanner tastatur, String[] fahrkarten, Double[] fahrkartenpreise) 
    {
        double zuZahlenderBetrag = 0;
        double tickets = 0;
        int ticket = 0;
        double preis = 0;
        boolean a = true;
        boolean b = true;
        boolean c = true;
while(c) {
	a = true;
	b = true;
	preis = 0;
	tickets = 0;
    	while(b) {
        System.out.println("\nW�hlen Sie ihre Wunschfahrkarte aus:");
    	 for(int i = 1; i<fahrkarten.length; i++)
    	 {
    		 System.out.println("	" + i + ".	" + fahrkarten[i] + "	|	"  + fahrkartenpreise[i] + "�");
    	 }
    	 System.out.print("	11.	Bezahlen");
    	 System.out.print("\nIhre Wahl: ");
         ticket = tastatur.nextInt();
    
         switch(ticket){
         case 1:
        	 b = false;
        	 preis = fahrkartenpreise[1];
        	 break;
         case 2:
        	 b = false;
        	 preis = fahrkartenpreise[2];
        	 break;
         case 3:
        	 b = false;
        	 preis = fahrkartenpreise[3];
        	 break;
         case 4:
        	 b = false;
        	 preis = fahrkartenpreise[4];
        	 break;
         case 5:
        	 b = false;
        	 preis = fahrkartenpreise[5];
        	 break;
         case 6:
        	 b = false;
        	 preis = fahrkartenpreise[6];
        	 break;
         case 7:
        	 b = false;
        	 preis = fahrkartenpreise[7];
        	 break;
         case 8:
        	 b = false;
        	 preis = fahrkartenpreise[8];
        	 break;
         case 9:
        	 b = false;
        	 preis = fahrkartenpreise[9];
        	 break;
         case 10:
        	 b = false;
        	 preis = fahrkartenpreise[10];
        	 break;
         case 11:
        	 b = false;
        	 c = false;
        	 a = false;
        	 break;
         default:
        	 System.out.println("\nFalsche Eingabe");
        	 
        	 break;
         }
    	}
         while(a) {
         System.out.print("\nAnzahl der Tickets: ");
         tickets = tastatur.nextInt();
         if (tickets < 1 || tickets > 10){
        	 System.out.println("W�hlen Sie bitte eine g�ltige anzahl von Tickets");
         }
         else {
        	 a = false;
         }
         }
        preis = preis * tickets;
     zuZahlenderBetrag = zuZahlenderBetrag + preis;
     
    }
	return zuZahlenderBetrag;
    }

    public static double fahrkartenBezahlen(double x, Scanner tastatur)
    {
    	double eingezahlterGesamtbetrag;
    	double eingeworfeneM�nze;
    	   eingezahlterGesamtbetrag = 0.0;
           while(eingezahlterGesamtbetrag < x)
           {
        	   
        	   System.out.printf("\nNoch zu zahlen: %.2f Euro", (x - eingezahlterGesamtbetrag));
        	   System.out.print("\nEingabe (mind. 1Ct, h�chstens 2 Euro): ");
        	   eingeworfeneM�nze = tastatur.nextDouble();
               eingezahlterGesamtbetrag += eingeworfeneM�nze;
           }
		return eingezahlterGesamtbetrag;

    }

       
public static void fahrkartenAusgeben()
{
	int millisekunde = 90;
	 System.out.println("\nFahrschein wird ausgegeben");
     for (int i = 0; i < 26; i++)
     {
        System.out.print("=");
        warte(millisekunde);
     }
     System.out.println("\n\n");
}

public static void warte(int x)
{
	try {
		Thread.sleep(x);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
   
public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag)
{
	double r�ckgabebetrag;
	
    r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    if(r�ckgabebetrag > 0.00)
    {
 	   System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
 	   System.out.println("wird in folgenden M�nzen ausgezahlt:");
 	   muenzeausgeben(r�ckgabebetrag);
            }

    System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                       "vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir w�nschen Ihnen eine gute Fahrt.");
	
    
}

public static void muenzeausgeben(double r�ckgabebetrag)
{
	while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
    {
 	  System.out.println("2 EURO");
          r�ckgabebetrag -= 2.0;
    }
    while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
    {
 	  System.out.println("1 EURO");
          r�ckgabebetrag -= 1.0;
    }
    while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
    {
 	  System.out.println("50 CENT");
          r�ckgabebetrag -= 0.5;
    }
    while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
    {
 	  System.out.println("20 CENT");
          r�ckgabebetrag -= 0.2;
    }
    while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
    {
 	  System.out.println("10 CENT");
          r�ckgabebetrag -= 0.1;
    }
    while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
    {
 	  System.out.println("5 CENT");
          r�ckgabebetrag -= 0.05;
    }
    while(r�ckgabebetrag >= 0.02)// 2 CENT-M�nzen
    {
 	  System.out.println("2 CENT");
          r�ckgabebetrag -= 0.02;
    }
    while(r�ckgabebetrag >= 0.01)// 1 CENT-M�nzen
    {
 	  System.out.println("1 CENT");
          r�ckgabebetrag -= 0.01;
    }

}
       
    
}